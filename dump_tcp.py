#!/usr/bin/env python2.7
# *-* encoding: utf-8 *-*

SYN_SENT = 'syn_sent'
SYN_RECIEVED = 'syn_recieved'
ESTABILISHED = 'estabilished'
FIN_INITIATOR = 'fin_initiator'
FIN_RESPONDER = 'fin_responder'
CLOSED = 'closed'
BROKEN = 'broken'

# Правила
STRICT = 0
AT_LIST = 1

# Флаги
ACK = 0x10
RST = 0x4
SYN = 0x2
FIN = 0x1

# Эта функция сделана для использования вместе со scapy
def has_flags(packet, rule):
    if rule[0] == STRICT:
        return packet.flags == rule[1]
    elif rule[0] == AT_LIST:
        return (packet.flags & rule[1]) == rule[1]
