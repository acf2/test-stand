#!/usr/bin/env python2.7
# *-* encoding: utf-8 *-*

import re
import os
import sys
import struct
from functools import reduce
from scapy.all import *
from dump_tcp import *
from specific_packing import *

# Типы пакетов в расписании
# Формат описан в docs, часть III
TYPE_COMMON = 0
TYPE_STARTING = 1
TYPE_ENDING = 2

def parse_dump(dump_file, tempfile):
    '''
    Эта функция парсит дамп, чтобы создать временный файл индексов
    Ей нужен dump_file - PcapReader и tempfile - имя временного файла
    '''

    ip_index = {} # {ip_address: ip_id}
    session_index = {} # {tetrad: [{'state': state, 'indices':[indices]}, ...]}
    count = [0]

    # NOTE: Python 2.7 does not have 'nonlocal' keyword. It's a pity
    next_id = {'ip': 0, 'session': 0, 'packet': 0}

    # Здесь реализована модель чуть проще, чем настоящий TCP
    # В предположении, что дамп корректен, можно сэкономить некоторое количество логики:
    # Допустим, можно считать, что после того, как обе стороны отправили FIN соединение уже закрыто.
    # Но тут появляется оверхэд - как понимать, какая сторона уже отправила FIN, а какая - нет?
    # Для этого и появилась дифференциация FIN_INITIATOR и FIN_RESPONDER
    # FIN_INITIATOR - инициатор сессии отправил FIN, но только он
    # FIN_RESPONDER - принимающая сторона сессии отправила FIN, но только она

    def initiate_session(session_index, packet, packet_number):
        # Первый шаг в TCP handshake
        src_ip = packet[IP].src
        src_port = packet[TCP].sport
        dst_ip = packet[IP].dst
        dst_port = packet[TCP].dport
        # Проверка что эти IP адреса есть в таблице, иначе добавление
        if not src_ip in ip_index:
            ip_index[src_ip] = next_id['ip']
            next_id['ip'] += 1
        if not dst_ip in ip_index:
            ip_index[dst_ip] = next_id['ip']
            next_id['ip'] += 1

        tetrad = (ip_index[src_ip], src_port, ip_index[dst_ip], dst_port)
        # Зачем номер пакета? Чтобы узнать кто и когда начал сессию.
        if tetrad in session_index:
            session_index[tetrad].append({'state': SYN_SENT, 'indices':[packet_number], 'id': next_id['session']})
        else:
            session_index[tetrad] = [{'state': SYN_SENT, 'indices':[packet_number], 'id': next_id['session']}]

        next_id['session'] += 1

    def respond_to_initiated_session(session_index, packet, packet_number):
        if not packet[IP].src in ip_index or not packet[IP].dst in ip_index:
            return

        # Второй шаг в TCP handshake
        # reverse tetrad в смысле того, что dst и src расположены наоборот
        # Т.е. пытаемся найти уже начатую сессию, на которуй стоило бы послать подтверждение
        rev_tetrad = (ip_index[packet[IP].dst], packet[TCP].dport, ip_index[packet[IP].src], packet[TCP].sport)
        if rev_tetrad in session_index:
            last_same_session = session_index[rev_tetrad][len(session_index[rev_tetrad]) - 1]
            if last_same_session['state'] == SYN_SENT:
                last_same_session['state'] = SYN_RECIEVED
                # last_same_session['indices'].append(packet_number) # is not nessesary

    def common_packet(session_index, packet, packet_number):
        # Для "обычных" пакетов. В смысле с полезной нагрузкой.

        if not packet[IP].src in ip_index or not packet[IP].dst in ip_index:
            return

        tetrad = (ip_index[packet[IP].src], packet[TCP].sport, ip_index[packet[IP].dst], packet[TCP].dport)
        if tetrad in session_index:
            last_same_session = session_index[tetrad][len(session_index[tetrad])-1]
            if last_same_session['state'] == SYN_RECIEVED:
                last_same_session['state'] = ESTABILISHED
            elif last_same_session['state'] == ESTABILISHED and Raw in packet:
                last_same_session['indices'].append(packet_number)
            return
        tetrad = tetrad[-2:] + tetrad[:2]
        if tetrad in session_index:
            last_same_session = session_index[tetrad][len(session_index[tetrad]) - 1]
            if last_same_session['state'] == ESTABILISHED and Raw in packet:
                last_same_session['indices'].append(packet_number)
            return

    def reset_session(session_index, packet, packet_number):
        if not packet[IP].src in ip_index or not packet[IP].dst in ip_index:
            return

        tetrad = (ip_index[packet[IP].src], packet[TCP].sport, ip_index[packet[IP].dst], packet[TCP].dport)
        if tetrad in session_index:
            last_same_session = session_index[tetrad][len(session_index[tetrad]) - 1]
            if last_same_session['state'] != CLOSED:
                last_same_session['state'] = BROKEN
                last_same_session['indices'].append(packet_number)
            return
        tetrad = tetrad[-2:] + tetrad[:2]
        if tetrad in session_index:
            last_same_session = session_index[tetrad][len(session_index[tetrad]) - 1]
            if last_same_session['state'] != CLOSED:
                last_same_session['state'] = BROKEN
                last_same_session['indices'].append(packet_number)
            return

    def finish_session(session_index, packet, packet_number):
        if not packet[IP].src in ip_index or not packet[IP].dst in ip_index:
            return

        tetrad = (ip_index[packet[IP].src], packet[TCP].sport, ip_index[packet[IP].dst], packet[TCP].dport)
        if tetrad in session_index:
            last_same_session = session_index[tetrad][len(session_index[tetrad]) - 1]
            if last_same_session['state'] == ESTABILISHED:
                last_same_session['state'] = FIN_INITIATOR
            elif last_same_session['state'] == FIN_RESPONDER:
                last_same_session['state'] = CLOSED
                last_same_session['indices'].append(packet_number)
            return
        tetrad = tetrad[-2:] + tetrad[:2]
        if tetrad in session_index:
            last_same_session = session_index[tetrad][len(session_index[tetrad]) - 1]
            if last_same_session['state'] == ESTABILISHED:
                last_same_session['state'] = FIN_RESPONDER
            elif last_same_session['state'] == FIN_INITIATOR:
                last_same_session['state'] = CLOSED
                last_same_session['indices'].append(packet_number)
            return

    def make_tempfile(session_index, tempfile):
        def make_tempfile_record(packet_id, session_id, packet_type):
            # Формат описан в docs, часть I
            return struct.pack('=I', packet_id) + struct.pack('=H', session_id) + pack_byte(packet_type)

        # Многофазная сортировка
        # Простое слияние и сортировка большого массива не используются
        # потому что в каждой сессии первый и последний пакеты - особо важные (необычные)
        # Их надо учитывать
        key_tetrads = [tetrad for tetrad in session_index] # тетрады (ip1, port1, ip2, port2), определяющие наборы сессий
        session_counters = [0 for i in xrange(len(session_index))] # Номер сессии для определённой тетрады
        packet_counters = [0 for i in xrange(len(session_index))] # Номер пакета в сессии для некоторой тетрады
        empty = [True for i in xrange(len(session_index))] # "Рассмотрены ли все пакеты в наборе сессий для этой тетрады?"
        
        # На всякий случай разберём выражение: session_index[key_tetrads[i]][session_counters[i]]['indices'][packet_counters[i]]
        # session_index - словарь {tetrad: [session1, session2, ...]}
        # В key_tetrads - хранятся все тетрады для возможности доступа по индексу
        # В session_counters хранится номер данной конкретной сессии для i-ой тетрады
        # session_index[key_tetrads[i]][session_counters[i]] - словарь {'state': state, 'indices': [packet_index1, packet_index2, ...]}
        # В packet_counters[i] храниться номер индекса ("индекс индекса") для данной тетрады, данной сессии
        # Как результат такая конструкция вернёт глобальный индекс пакета

        def initiate_counters():
            for i in xrange(len(session_counters)):
                while session_counters[i] < len(session_index[key_tetrads[i]]):
                    if session_index[key_tetrads[i]][session_counters[i]]['state'] == CLOSED:
                        empty[i] = False
                        break
                    session_counters[i] += 1

        def get_minimal_entry():
            min_index = None
            min_value = None

            for i in xrange(len(session_index)):
                if not empty[i]:
                    packet_id = session_index[key_tetrads[i]][session_counters[i]]['indices'][packet_counters[i]]
                    session_id = session_index[key_tetrads[i]][session_counters[i]]['id']
                    if min_value is None or packet_id < min_value[0]:
                        min_index = i

                        if packet_counters[i] == 0:
                            # Это первый пакет в данной сессии
                            min_value = (packet_id, session_id, TYPE_STARTING)
                        elif packet_counters[i] == len(session_index[key_tetrads[i]][session_counters[i]]['indices']) - 1:
                            # Это последний пакет в данной сессии
                            min_value = (packet_id, session_id, TYPE_ENDING)
                        else:
                            # Это обычный пакет
                            min_value = (packet_id, session_id, TYPE_COMMON)

            if min_value is None:
                return None
            # Взяли пакет? Нужно подготовить следующий
            packet_counters[min_index] += 1
            # Если в этой сессии больше нет пакетов
            if packet_counters[min_index] == len(session_index[key_tetrads[min_index]][session_counters[min_index]]['indices']):
                session_counters[min_index] += 1
                packet_counters[min_index] = 0
                # Если для этой тетрады больше нет сессий
                if session_counters[min_index] == len(session_index[key_tetrads[min_index]]):
                    session_counters[min_index] = 0
                    empty[min_index] = True

            return min_value

        initiate_counters()
        with open(tempfile, 'wb') as tfile:
            while True:
                p = get_minimal_entry()
                if p is None:
                    break
                tfile.write(make_tempfile_record(p[0], p[1], p[2]))

    ### Код высокого уровня здесь ###

    dump_reader = PcapReader(dump_file)

    # Основная идея в том, чтобы количество условий было минимально,
    # а новую логику можно было бы добавлять сравнительно легко
    # Поэтому pattern-matching
    actions = [((STRICT, SYN), initiate_session),
               ((STRICT, SYN | ACK), respond_to_initiated_session),
               ((AT_LIST, RST), reset_session),
               ((AT_LIST, FIN), finish_session),
               ((AT_LIST, ACK), common_packet)]

    packet_number = 0
    while True:
        packet = dump_reader.read_packet()
        if packet is None:
            break

        if TCP in packet:
            for rule, action in actions:
                if has_flags(packet[TCP], rule):
                    action(session_index, packet, packet_number)
                    break
        packet_number += 1


    make_tempfile(session_index, tempfile)

    return (ip_index, session_index)

def make_schedule(dump_file, tempfile, (ip_index, session_index), outfile):
    def make_address_book(ip_index):
        # Формат описан в docs, часть II, ADDRESS BOOK
        result = struct.pack('=I', len(ip_index))
        for ip_address, ip_id in ip_index.iteritems():
            result += pack_byte(ip_id) + pack_ip(ip_address)
        return result

    def make_session_book(session_index):
        # Формат описан в docs, часть II, SESSION BOOK
        result = struct.pack('=I', reduce((lambda x,y: x+y), [len(s) for t, s in session_index.iteritems()]))
        for tetrad, session_sequence in session_index.iteritems():
            for session in session_sequence:
                result += struct.pack('=H', session['id']) + pack_byte(tetrad[0]) + struct.pack('=H', tetrad[1]) + pack_byte(tetrad[2]) + struct.pack('=H', tetrad[3])
        return result

    def extract_tempfile_entry(descriptor):
        buf = descriptor.read(4)
        if len(buf) != 4:
            return None
        packet_id = struct.unpack('=I', buf)[0]
        session_id = struct.unpack('=H', descriptor.read(2))[0]
        packet_type = unpack_byte(descriptor.read(1))
        return (packet_id, session_id, packet_type)

    # Формат описан в docs, часть II, RECORD
    def make_common_record(session_id, ip_id, payload):
        return struct.pack('=H', session_id) + pack_byte(TYPE_COMMON) + pack_byte(ip_id) + struct.pack('=I', len(payload)) + payload

    def make_starting_record(session_id, ip_id):
        return struct.pack('=H', session_id) + pack_byte(TYPE_STARTING) + pack_byte(ip_id)

    def make_ending_record(session_id):
        return struct.pack('=H', session_id) + pack_byte(TYPE_ENDING)

    ### Код высокого уровня здесь ###

    with open(outfile, 'wb') as schedule, open(tempfile, 'rb') as tfile:
        schedule.write(make_address_book(ip_index) + make_session_book(session_index))

        actions = {TYPE_COMMON: lambda s, i, p: schedule.write(make_common_record(s, i, p)),
                   TYPE_STARTING: lambda s, i, p: schedule.write(make_starting_record(s, i)),
                   TYPE_ENDING: lambda s, i, p: schedule.write(make_ending_record(s))}

        dump_reader = PcapReader(dump_file)
        packet_counter = 0
        packet = dump_reader.read_packet()
        while True:
            tempfile_entry = extract_tempfile_entry(tfile)
            if tempfile_entry is None:
                break
            (packet_id, session_id, packet_type) = tempfile_entry

            while packet_counter < packet_id:
                packet = dump_reader.read_packet()
                packet_counter += 1

            ip_id = ip_index[packet[IP].src]
            payload = packet[Raw].load if Raw in packet else ''

            # Выбираем как записывать
            actions[packet_type](session_id, ip_id, payload)

def parse_arguments(argv):
    pass

def main(argv):
    if len(argv) < 3:
        print "Usage: python2.7 %s <dump file> <schedule file> [<tempfile>]" % argv[0] if len(argv) > 0 else "parser"
        return 0

    dump_file = argv[1]
    schedule_file = argv[2]
    if len(argv) > 3:
        tempfile = argv[3]
    else:
        tempfile = '/tmp/pcap_parser_tempfile'

    make_schedule(dump_file, tempfile, parse_dump(dump_file, tempfile), schedule_file)

    return 0

if __name__ == '__main__':
    exit(main(sys.argv))
