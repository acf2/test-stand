#!/usr/bin/env python2.7
# *-* encoding: utf-8 *-*

import re
import sys
import socket
import struct
from specific_packing import *

# Типы пакетов в расписании
# Формат описан в docs, часть III
TYPE_COMMON = 0
TYPE_STARTING = 1
TYPE_ENDING = 2

class Schedule:
    def __init__(self, schedule_file):
        self.filename = schedule_file
        self.descriptor = open(self.filename, 'rb')
        self.address_book = {}
        self.session_book = {}

        # Адресная книга будет использована для индексации адресов
        address_size = struct.unpack('=I', self.descriptor.read(4))[0]
        for i in xrange(address_size):
            ip_id = unpack_byte(self.descriptor.read(1))
            ip_address = unpack_ip(self.descriptor.read(4))
            self.address_book[ip_id] = ip_address

        # Сессионная книга хранит пары сокетов, что были использованы в сессии
        # Причём вместо IP адресов используются их ID
        session_size = struct.unpack('=I', self.descriptor.read(4))[0]
        for i in xrange(session_size):
            session_id = struct.unpack('=H', self.descriptor.read(2))[0]
            ip1_id = unpack_byte(self.descriptor.read(1))
            port1 = struct.unpack('=H', self.descriptor.read(2))[0]
            ip2_id = unpack_byte(self.descriptor.read(1))
            port2 = struct.unpack('=H', self.descriptor.read(2))[0]
            self.session_book[session_id] = (ip1_id, port1, ip2_id, port2)

        self.eof = False

        # Вот эта конструкция устанавливает пакетный буфер
        # Он нужен как минимум для прозрачного EOF
        self.next_packet = None
        self.pop_packet()

    def pop_packet(self):
        result = self.next_packet

        temp = self.descriptor.read(2)
        if len(temp) != 2:
            self.eof = True
            return result

        session_id = struct.unpack('=H', temp)[0]
        packet_type = unpack_byte(self.descriptor.read(1))
        if packet_type == TYPE_COMMON:
            sip_id = unpack_byte(self.descriptor.read(1))
            payload_length = struct.unpack('=I', self.descriptor.read(4))[0]
            payload = self.descriptor.read(payload_length)
            self.next_packet = (session_id, packet_type, sip_id, payload)
        elif packet_type == TYPE_STARTING:
            sip_id = unpack_byte(self.descriptor.read(1))
            self.next_packet = (session_id, packet_type, sip_id)
        elif packet_type == TYPE_ENDING:
            self.next_packet = (session_id, packet_type)

        return result

    def close():
        if not self.descriptor.closed:
            seld.descriptor.close()

class HostMapping:
    # Пары адресов, чтобы сопоставлять адресам из дампа реальные адреса тестовой сети
    # Также нужно выбирать порты
    # Сервисные - неизменны, остальные - рандомизируются
    def __init__(self, dump_ip, (remote_ip, remote_dump_ip), service_ports = []):
        self.dump_ip = dump_ip
        self.remote_ip = remote_ip
        self.remote_dump_ip = remote_dump_ip
        self.service_ports = service_ports
        if len(self.service_ports) > 0:
            self.listener = True
        else:
            self.listener = False

def simulate_network_host(host_mapping, schedule):

    ip_index = {v: k for k, v in schedule.address_book.iteritems()}
    local_id = ip_index[host_mapping.dump_ip]
    remote_id = ip_index[host_mapping.remote_dump_ip]

    listening_sockets = {} # {port: listening_socket}
    connections = {} # {session_id: opened_socket}

    # Инициализировать все слушающие сокеты (если есть)
    if host_mapping.listener == True:
        for sp in host_mapping.service_ports:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind(('0.0.0.0', sp))
            sock.listen(5)
            listening_sockets[sp] = sock

    # send/recive data
    def sr_data(packet):
        (session_id, packet_type, sip_id, payload) = packet

        # Источником пакета является эта машина?
        if sip_id == local_id:
            if not session_id in connections:
                raise RuntimeError("Schedule error: session is not opened")

            # Отправить пакет
            connections[session_id].send(payload)
        elif sip_id == remote_id:
            if not session_id in connections:
                raise RuntimeError("Schedule error: session is not opened")

            # Принять пакет
            recieved = connections[session_id].recv(len(payload))
            if payload != recieved:
                raise RuntimeError("Schedule error: recieved packet not expected\nExpected: '%s'\nRecieved: '%s'" % (payload, recieved))

    def open_socket(packet):
        # Будет бесконечно пытатся подсоединиться
        def definetly_connect(sock, address):
            not_connected = True
            while not_connected:
                try:
                    sock.connect(address)
                    not_connected = False
                except:
                    not_connected = True

        (session_id, packet_type, sip_id) = packet

        if session_id in connections:
            raise RuntimeError("Schedule error: reopening connection")

        if sip_id == local_id:
            session = schedule.session_book[session_id]
            temp_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            definetly_connect(temp_sock, (host_mapping.remote_ip, session[3] if local_id == session[0] else session[1]))
            connections[session_id] = temp_sock
        elif sip_id == remote_id:
            if host_mapping.listener == False:
                raise RuntimeError("Schedule error: client should not listen")
            session = schedule.session_book[session_id]
            port = session[3] if remote_id == session[0] else session[1]
            if not port in listening_sockets:
                raise RuntimeError("Schedule error: attempting to connect to nonlistening socket")
            connections[session_id] = listening_sockets[port].accept()[0]

    def close_socket(packet):
        (session_id, packet_type) = packet
        connections[session_id].close()

    actions = {TYPE_COMMON: sr_data,
               TYPE_STARTING: open_socket,
               TYPE_ENDING: close_socket}

    # Главный цикл - обработка пакетов
    while not schedule.eof:
        # packet = (session_id, type_of_packet, ...)
        packet = schedule.pop_packet()
        # Выбрать действие
        actions[packet[1]](packet)

    for session_id, sock in connections.iteritems():
        sock.close()

    for port, sock in listening_sockets.iteritems():
        sock.close()

def parse_arguments(argv, required):
    arguments = {} # {name: value}

    ip_regex = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
    local_mapping = r'^(' + ip_regex + r')$'
    remote_mapping = r'^(' + ip_regex + r'):(' + ip_regex + r')$'
    service_ports = r'^-s((?:\d+,)*\d+)?$'
    schedule_file = r'^-i(.+)?$'

    COMMON_STATE = 0
    EXPECTING_SERVICE_PORTS = 1
    EXPECTING_SCHEDULE_FILE = 2
    # В python2.x нет ключевого слова 'nonlocal'
    state = [0]

    def common_action(arg):
        def extract_local_mapping(groups):
            arguments['localmap'] = groups[0]

        def extract_remote_mapping(groups):
            arguments['remotemap'] = (groups[0], groups[1])

        def extract_service_status(groups):
            if groups[0] is None:
                state[0] = EXPECTING_SERVICE_PORTS
            else:
                arguments['serviceports'] = [int(i) for i in groups[0].split(',')]

        def extract_schedule_file(groups):
            if groups[0] is None:
                state[0] = EXPECTING_SCHEDULE_FILE
            else:
                arguments['schedule'] = groups[0]

        actions = [(local_mapping, extract_local_mapping),
                   (remote_mapping, extract_remote_mapping),
                   (service_ports, extract_service_status),
                   (schedule_file, extract_schedule_file)]

        for (regex, action) in actions:
            mch = re.match(regex, arg)
            if not mch is None:
                action(mch.groups())
                break

    def set_service_ports(arg):
        arguments['serviceports'] = [int(i) for i in arg.split(',')]
        state[0] = COMMON_STATE

    def set_schedule_file(arg):
        arguments['schedule'] = arg
        state[0] = COMMON_STATE

    actions = {COMMON_STATE: common_action,
               EXPECTING_SERVICE_PORTS: set_service_ports,
               EXPECTING_SCHEDULE_FILE: set_schedule_file}

    for i in xrange(1, len(argv)):
        actions[state[0]](argv[i])

    for req in required:
        if not req in arguments:
            return None
    return arguments

def main(argv):
    parsed_args = parse_arguments(argv, ['schedule', 'localmap', 'remotemap'])
    if parsed_args is None:
        print "Usage: python2.7 %s <mapped IP> <remote IP>:<IP mapped to remote> -i<schedule file> [-s <service port>[,<service port>[,...]]]\nIf at least one service port if specified, than script is running in 'server' mode\nOtherwise in 'client' mode" % argv[0] if len(argv) > 0 else "simulate_host"
        return 0

    schedule = Schedule(parsed_args['schedule'])
    host_mapping = HostMapping(parsed_args['localmap'], parsed_args['remotemap'], parsed_args['serviceports'] if 'serviceports' in parsed_args else [])
    simulate_network_host(host_mapping, schedule)

if __name__ == '__main__':
    exit(main(sys.argv))
