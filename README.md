# Test stand
For testing IDPS

## parse\_and\_translate.py
Creates schedule file from pcap.
Session schedule can be used for reproducing traffic with simulate\_host.py

*Examples*:
Parse dump and translate it to schedule file:
```console
$ python2.7 parser.py dump.pcap out.sch
```
Order of arguments is important.

## simulate\_host.py
Reproduces traffic from one host to another.
Can be runned in 2 modes: server or client.

Both should be launched with mapping: two IP addresses from dump should be binded via script options to real IP addresses (for this machine and remote one).
Server must also specify it's service ports. And if port number is less than 1024, 'server' script must be runned as root.

*Examples*:
Run simulation as client 192.168.1.2 that connects to server 10.10.10.0 (real machine 172.16.5.5 simulates the server).
Schedule file - out.sch
```console
$ python2.7 simulate_host.py 192.168.1.2 172.16.5.5:10.10.10.0 -i out.sch
```

Run simulation as server 10.10.10.0 that accepts connections from 192.168.1.2 and have services on ports 80, 31337.
Schedule file out2.sch and real 172.16.1.42 simulates client.
```console
$ python2.7 simulate_host.py -iout2.sch 10.10.10.0 172.16.1.42:192.168.1.2 -s 80,31337
```
Order of arguments does not matter.

