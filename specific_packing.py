#!/usr/bin/env python2.7
# *-* encoding: utf-8 *-*

import re
import struct

def pack_byte(num):
    return struct.pack('=c', chr(num))

def unpack_byte(byte):
    return ord(struct.unpack('=c', byte)[0])

def pack_ip(ip):
    mtch = re.match(r'^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$', ip)
    return ''.join([chr(int(s)) for s in mtch.groups()])

def unpack_ip(string):
    return '.'.join([str(ord(b)) for b in string])
